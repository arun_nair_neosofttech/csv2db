import pymysql
import csv


conn = pymysql.connect(host='localhost',
                       user='root',
                       password='root',
                       db='arun_db')

cur = conn.cursor()
with conn:
    # table already created
    # cur.execute(
    #     "CREATE TABLE biostats(name VARCHAR(10) NOT NULL, gender ENUM('M','F'), age INT, height INT, weight INT);")

    with open('biostats.csv', 'r') as cs:
        csvread = csv.reader(cs)
        next(csvread)
        for record in csvread:
            query = 'INSERT INTO biostats (name, gender, age, height, weight) VALUES (%s, %s, %s, %s, %s)'
            cur.execute(query, record)
        conn.commit()

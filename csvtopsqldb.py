import psycopg2
import csv
import os

os.chdir('Documents/aaroon/Python Assignments/Asgnt_4_1_csv2db')
conn = psycopg2.connect(
    database="postgres",
    host="localhost",
    port="5432",
    user="postgres",
    password="root"
)

cur = conn.cursor()
with conn:
    # table already created
    # cur.execute("CREATE TYPE gender as ENUM('M','F');")
    # cur.execute("CREATE TABLE biostats(name VARCHAR(20) NOT NULL, gender gender NOT NULL, age INT NOT NULL, height INT NOT NULL, weight INT NOT NULL;)")
    with open('biostats.csv', 'r') as file:
        fileread = csv.reader(file)
        next(fileread)  # skipping header
        for line in fileread:
            cur.execute("INSERT INTO biostats VALUES (%s,%s,%s,%s,%s)", line)
        conn.commit()
